#include "command.ih"

Command::Command()
:
    d_action(FROM_HOME),
    d_parent(0),
    d_homedirChar(Options::instance().homedirChar())
{
    imsg << "\n"
            "Command\n";

    concatArgs();       // concatenate arguments, separating them by /
                        // characters

    determineAction();

                        // store the individual arguments in the base class
                        // (string vector)
    String::split(this, d_arguments, s_separators);

    // When are the elements of the first argument changed into initial chars
    // of directory elements?
    // 1. if there is only one command line argument
    // 2. if the first argument is not to be interpreted as a name by itself
    // 3. if there's only one argument
    // Can't 2 and 3 be combined to: size() == 1 ?
//    if (!subSpecs && size() && ArgConfig::instance().nArgs() == 1)

    splitBase();            // split the base class if it contains only
                            // one string.

    if (ArgConfig::instance().option('V'))
    {
        cerr << "Parent nr: " << d_parent << "\n"
                "Action: " << s_action[d_action] << "\n";
        if (empty())
            cerr << "No initial directory specifier(s)";
        else
        {
            cerr << "Initial characters of directories: ";
            copy(begin(), end(), ostream_iterator<string>(cerr, " "));
        }
        cerr << endl;
    }
}











