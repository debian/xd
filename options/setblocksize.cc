#include "options.ih"

void Options::setBlockSize()
{
    string value;
    d_blockSize = d_arg.option(&value, 'b') ? stoul(value) : UINT_MAX;

        imsg << "Block size: " << d_blockSize << endl;
}
