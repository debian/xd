#include "options.ih"

// static
string Options::getHome()
{
    string homeDir;

    char *cp = getenv("HOME");              // determine the homedir

    if (!cp)                                // save it
        homeDir = '/';
    else
    {
        homeDir = cp;
        if (homeDir.back() != '/')          // ensure final / at the homedir
            homeDir += '/';
    }

    imsg << "Home directory: " << homeDir << endl;

    return homeDir;
}
