#include "options.ih"

void Options::versionHelp(char const *version, 
                          void (*usage)(string  const  &)) const
{
    streambuf *buf = cout.rdbuf(cerr.rdbuf());      // make sure that
    try                                             // versionHelp doesn't
    {                                               // write to cout

        d_arg.versionHelp(usage, version, 1);       // need at least 1 arg 
        cout.rdbuf(buf);                            // insert the help
    }
    catch(...)
    {
        cout.rdbuf(buf);                            // missing argument:
        throw;                                      // help is provided and
                                                    // ends the program
    }
    
}
