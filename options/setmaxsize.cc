#include "options.ih"

void Options::setMaxSize()
{
    string value;
    if (d_arg.option(&value, "history-maxsize"))
    {
        d_historyMaxSize = stoul(value);
        imsg << "History max size: " << d_historyMaxSize << " elements" << 
                                                                        endl;
    }
    else
    {
        d_historyMaxSize = UINT_MAX;
        imsg << "History max size: none" << endl;
    }
}
