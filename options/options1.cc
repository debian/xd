#define XERR
#include "options.ih"

Options::Options(char const *optString, 
                ArgConfig::LongOption const *const begin,  
                ArgConfig::LongOption const *const end,  
                int argc, char **argv, char const *version, 
                void (*usage)(std::string  const  &))
:
    d_arg(ArgConfig::initialize(optString, begin, end, argc, argv)),
    d_now(time(0)),
    d_separate(d_arg.option(0, "history-separate"))
{
    string_view last{ argv[argc - 1] }; // remove a last / from the last
    if (last.back() == '/')             // argv element
        argv[argc - 1][last.length() - 1] = 0;

    d_arg.setCommentHandling(ArgConfig::RemoveComment);

    imsg.reset(cerr);                   // mstream messages go to cerr
    imsg.setActive(d_arg.option('V'));

    imsg << "\n"
            "Options\n";

    fmsg.reset(cerr);

    d_homeDir = getHome();

    bool noInput = d_arg.option(0, "no-input") != 0;

    string msg = readConfigFile();

    s_input = not noInput and d_arg.option(0, "input") != 0;

    versionHelp(version, usage);

    if (not msg.empty())
        imsg << msg << endl;

    if (s_input)
        imsg << "shell-input a non selection/space character" << endl;
    else
        imsg << "xd ends at a non selection/space character" << endl;

                                // true, unless 'start-at root' was specified
    d_fromHome = s_fromHome[ find("start-at", s_startAt, s_endStartAt) ];

    d_allDirs = s_allDirs[ find("directories", s_dirs, s_endDirs) ];

    d_addRoot = s_addRoot[ find("add-root", s_triState, s_endTriState) ];


    setHistory();                           // history filename
    setMaxSize();                           // history max size
    setHistoryLifetime();
    setPosition();                          // history position
    setBlockSize();
}       







