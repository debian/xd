inline TriState Options::addRoot() const
{
    return d_addRoot;
}

inline bool Options::all() const
{
    return d_arg.option('a');
}

inline bool Options::allDirs() const
{
    return d_allDirs;
}

inline FBB::Ranger<char const **> Options::args() const
{
    return FBB::ranger(d_arg.argPointers(), d_arg.nArgs());
}

inline size_t Options::blockSize() const
{
    return d_blockSize;
}


inline bool Options::fromHome() const
{
    return d_fromHome;
}

inline bool Options::generalized() const
{
    return d_arg.option('g');
}

inline size_t Options::historyLifetime() const
{
    return d_historyLifetime;
}

inline size_t Options::historyMaxSize() const
{
    return d_historyMaxSize;
}

inline std::string const &Options::history() const
{
    return d_history;
}

inline std::string const &Options::homeDir() const
{
    return d_homeDir;
}

inline size_t Options::icase() const
{
    return d_arg.option('i');
}

// static
inline bool Options::input()
{
    return s_input;
}

inline auto Options::ignore() const
{
    return d_arg.beginEndRE("^\\s*ignore\\s+\\S+\\s*$");
}

inline size_t Options::now() const
{
    return d_now;
}

inline Position Options::historyPosition() const
{
    return d_historyPosition;
}

inline bool Options::separate() const
{
    return d_separate;
}

inline bool Options::traditional() const
{
    return d_arg.option(0, "traditional");
}


inline std::string const &Options::triStateStr() const
{
    return s_triState[d_addRoot];
}







