#include "alternatives.ih"

void Alternatives::viable()
{
    startDir();

    imsg << boolalpha  << "Additional search from $HOME: " << 
                                                d_fromHome << '\n' <<
                "Search all directories: " << d_allDirs << '\n' <<
                "Add root search if search from  $HOME fails: " <<
                                        d_options.triStateStr() << '\n' <<
                "Start dir = " << d_initialDir << endl;

    findAlternatives();                         // sets d_result to DIRECT
                                                // or MULTIPLE.
    if (d_result == DIRECT)
        return;

    sort(begin(), begin() + d_nInHistory);
    sort(begin() + d_nInHistory, end());
}
