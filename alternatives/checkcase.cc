#include "alternatives.ih"

// static
void Alternatives::checkCase(size_t *idx, string &searchCmd)
{
                                        // at an odd number of --icase specs:
    if ((Options::instance().icase() & 1) != 0)   
    {                                   // use case insensitive matching

        string mold("[..]");            // the pattern to contain, e.g., [aA]

        int ch = searchCmd[*idx];       // the char at 'idx'

        if (isalpha(ch))                // if it's a letter
        {
            mold[1] = tolower(ch);      // then fill the lc/uc variants, 
            mold[2] = toupper(ch);

            searchCmd.replace(*idx, 1, mold);   // replace char @idx by [xX],
            *idx += 4;                  // and shift the next idx forwar by 4
            return;
        }
    }

    ++*idx;                             // no letter or case sens. match: inc
}
