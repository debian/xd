#include "alternatives.ih"

Alternatives::Alternatives()
:
    d_options(Options::instance()),
    d_nInHistory(0),
    d_fromHome(d_options.fromHome()),   // true unless 'start-at home'
    d_allDirs(d_options.allDirs()),
    d_addRoot(d_options.addRoot()),
    d_result(NONE)
{
    imsg << "\n"
            "Alternatives\n";
}
