#include "alternatives.ih"

    // realpath shoyld return a string, not a char *, so the responsibility 
    // of allocated memory isn't xd's responsibility anymore.

// static
string Alternatives::getCwd()
{
    char *currentPath = realpath(".", 0);

    if (currentPath == 0)
        fmsg << "Can't determine the current working dir." << endl;

    string ret{ currentPath };

    free(currentPath);

    return ret;
}
