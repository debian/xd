#include "alternatives.ih"

// see globpattern.cc:
//      if the head contains a / it is not tested.
//      If the tail starts with /, that char is ignored.

void Alternatives::prepareDir(string &dir, size_t *idx, string &searchCmd)
{
                                            // update searchCmd when case
    checkCase(idx, searchCmd);              // insensitive matching is used

        // create a pattern from dir + initial substring
    string head = searchCmd.substr(0, *idx);

    if (head.find('/') != string::npos)     // ignore if head has a /, caught
        throw false;                        // by generalizedAlternatives

    dir += head;
    dir += "*/";                        // this pattern must exist
}
