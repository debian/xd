#include "alternatives.ih"

void Alternatives::findAlternatives()
{
    if (not d_options.all())        // skip 'ignore' specs in the config file
        setIgnored();

    findDirs();                     // find alternative dirs at 'startDir'
}




