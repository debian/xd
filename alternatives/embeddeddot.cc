#include "alternatives.ih"

    // true: dirEntry has a /./ pattern

bool Alternatives::embeddedDot(string const &dirEntry) const
{
    if (
        dirEntry.find("/./") != string::npos   // ignore */./* patterns
        or
            find_if(
                d_ignore.begin(), d_ignore.end(),
                [&](string const &ignore)
                {
                    return matchIgnore(ignore, dirEntry);
                }
            )
            != d_ignore.end()
    )
    {
        imsg << "ignored" << endl;
        return true;
    }

    return false;
}
