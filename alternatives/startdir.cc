#include "alternatives.ih"

void Alternatives::startDir()
{
    bool fromConfig = false;

    switch (d_command.action())
    {
        case Command::FROM_CONFIG:
            d_initialDir = d_fromHome ? d_options.homeDir() : "/"s;
            fromConfig = true;
        break;

        case Command::FROM_HOME:
            d_initialDir = d_options.homeDir();
        break;

        case Command::FROM_ROOT:
            d_initialDir = "/"s;
        break;

        case Command::FROM_CWD:
            d_initialDir = getCwd();
        break;

        case Command::FROM_PARENT:
            d_initialDir = getCwd();
            size_t pos = d_initialDir.length();
                                            // remove parent() path elements
            for (size_t idx = d_command.parent(); pos && idx--; )
                pos = d_initialDir.rfind('/', pos - 1);

            if (pos != string::npos)        // parent() elements found
                d_initialDir.resize(pos);   // rmove the tail
            else
                d_initialDir = '/';         // fewer subd_initialDirs: use /
        break;
    }

    if (d_addRoot != NEVER and (not d_fromHome || not fromConfig))
    {
        imsg << "Search does not start at the home-dir: "
                "no additional search from the root" << endl;

        d_addRoot = NEVER;
    }

    if (d_initialDir.back() != '/')         // all d_initialDirs end in /
        d_initialDir += '/';

    imsg << "Resolved starting the initial dir as: " << d_initialDir << endl;
}
