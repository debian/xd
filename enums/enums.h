#ifndef INCLUDED_ENUMS_H_
#define INCLUDED_ENUMS_H_

enum TriState           // used by Alternatives
{
    NEVER,
    IF_EMPTY,
    ALWAYS
};

enum Position           // used by History
{
    TOP,
    BOTTOM
};

enum Result
{
    NONE,                   // no solutions
    DIRECT,                 // direct CD (one solution)
    MULTIPLE,               // RECEIVED_ALTERNATIVES,  
};


#endif
