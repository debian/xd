#include "selector.ih"

string Selector::s_allChars{ "123456790"
                             "abcdefghijklmnopqrstuvwxyz"
                             "ABCDEFGHIJKLMNOPQRSTUVWXYZ" };

string Selector::s_inc{ "+.>" };
string Selector::s_dec{ "-,<" };

