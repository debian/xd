#include "selector.ih"

// static
void Selector::input(string const &text)
{
    Tty tty{ Tty::OFF };

    for (char ch: text)
    {
        if (ioctl(0, TIOCSTI, &ch) == -1)
            imsg << __FILE__ ": ioctl returned -1: " << errnodescr << endl;
    }
}
