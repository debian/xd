#include "selector.ih"

void Selector::select()
{
    if (d_direct)
        chdir();
    else
    {
        switch (d_alternatives.size())
        {
            case 0:                     // no alternatives, throw avoids
            throw static_cast<int>(NO_SOLUTIONS);   // updating the history
    
            case 1:                     // one alternative: do the cd
                imsg << endl;
                d_index = 0;
                chdir();
            break;
    
            default:                    // otherwise: show all alternatives
                showAlternatives();     // show the alternatives
            break;
        }
    }
}
