#include "selector.ih"

void Selector::chdir()
{
    d_returnValue = CD;

    string const &dir{ d_alternatives[d_index] };

    if (d_ioctl)                // --input was specified.
    {
        imsg << "input: cd " << dir << endl;
        input("cd " +  dir + '\n');
    }
    else
    {
        imsg << "direct cout: " << dir << endl;
        cout << dir << '\n';
    }
                                        // Alternatives handles the DIRECT
    d_alternatives.update(d_index);     // update
}
