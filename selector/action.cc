#include "selector.ih"

Selector::Action Selector::action(Block const &block)
{
    OneKey oneKey;

    int ch = oneKey.get();                      // get the reply

    imsg << "Selector::action received oneKey " << static_cast<char>(ch) << 
                                                                        endl;

    if (accept(ch, s_inc, block.prompt))        // go to the next block
        return INC;

    if (accept(ch, s_dec, block.prompt))        // to the previous block
        return DEC;

    string blockChars = s_allChars.substr(0, block.end - block.begin);

    if (size_t idx = blockChars.find(ch); idx != string::npos)
        d_index = block.begin + idx;
    else 
        d_index = block.end;

    if (isspace(ch))                            // change space chars to ' ' 
        ch = ' ';                               // to prevent extra prompt

    if (d_index < block.end)                    // directory was selected
        chdir();                                // chdir to the alt.[d_index]
    else
        noCD(ch);                               // at --input: shell-input 
                                                // the entered character
    return Action::INPUT;
}
