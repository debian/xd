#include "selector.ih"

void Selector::show(Block const &block) const
{
    imsg << endl;

    for (
        size_t chIdx = 0, idx = block.begin, 
                separateAt = d_alternatives.separateAt(); 
            idx != block.end; 
                ++idx, ++chIdx
    )
    {
        if (idx == separateAt)
            cerr << '\n';
        cerr << setw(2) << s_allChars[chIdx] << ": " << 
                           d_alternatives[idx] << '\n';
    }

    if (not block.prompt.empty())
        cerr << ' ' << block.prompt << '\n';
}

