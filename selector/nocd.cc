#include "selector.ih"

void Selector::noCD(int ch)
{
    d_returnValue = ch == ' ' ? NO_SELECTION : COMMAND;

    if (not d_ioctl)
        cout << ".\n";
    else if (d_returnValue == COMMAND)   // --input was specified.
        input(string{ static_cast<char>(ch) });
}
