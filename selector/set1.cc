#include "selector.ih"

void Selector::set(Block &nextBlok, size_t lastEnd) const
{
    nextBlok.begin = lastEnd;
    nextBlok.end = lastEnd + d_blockSize;
}
