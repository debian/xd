#include "history.ih"

void History::load()
{
    ifstream in{ d_historyFilename };
    if (!in)
    {
        imsg << "History file `" << d_historyFilename << "' not readable" << endl;
        return;
    }

    imsg << "History file `" << d_historyFilename << '\'' << endl;

    for_each(
        istream_iterator<HistoryInfo>(in),
        istream_iterator<HistoryInfo>(),
        [&](HistoryInfo  const &historyInfo)
        {
            maybeInsert(historyInfo, d_history, d_options.historyLifetime());
        }
    );
}
