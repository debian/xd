inline History::HistoryInfo::HistoryInfo(size_t time, size_t count,
                                                    std::string const &path)
:
    time(time),
    count(count),
    path(path)
{}

inline bool History::find(std::string const &path) const
{
    return findIter(path) != d_history.end();
}

inline std::vector<History::HistoryInfo>::const_iterator 
        History::findIter(std::string const &path) const
{
    return
        find_if(
            d_history.begin(), d_history.end(),
            [&](HistoryInfo const &history)
            {
                return history.path == path;
            }
        );
}

inline Position History::position() const
{
    return d_options.historyPosition();
}

inline bool History::rotate() const
{
    return not d_historyFilename.empty() && 
           d_options.historyPosition() == BOTTOM;
}

inline std::ostream &operator<<(std::ostream &out,
                                History::HistoryInfo const &hi)
{
    return out << hi.time << ' ' << hi.count << ' ' << hi.path;
}




