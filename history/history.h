#ifndef INCLUDED_HISTORY_
#define INCLUDED_HISTORY_

#include <string>
#include <vector>
#include <iostream>
#include <algorithm>

#include "../enums/enums.h"
#include "../options/options.h"

class History
{
    struct HistoryInfo
    {
        size_t      time;
        size_t      count;
        std::string path;

        HistoryInfo() = default;
        HistoryInfo(size_t time, size_t count, std::string const &path);
    };

    friend std::istream &operator>>(std::istream &in, HistoryInfo &hl);
    friend std::ostream &operator<<(std::ostream &in,
                                                HistoryInfo const &hl);

    Options const &d_options;

    std::string d_historyFilename;              // name of the history file

    size_t d_now;                   // current time

    std::vector<HistoryInfo> d_history;

    static char s_defaultHistory[];

    public:
        History();

        Position position() const;

        void setLocation(size_t nInHistory);
        void save(std::string const &choice);
        bool rotate() const;

                                        // see if a path is in the history
        bool find(std::string const &path) const;

    private:
        std::vector<HistoryInfo>::const_iterator findIter(
                                            std::string const &path) const;
        void load();

        static void maybeInsert(HistoryInfo  const &historyInfo,
                                std::vector<HistoryInfo> &history,
                                size_t now);
        static bool compareTimes(HistoryInfo const &first,
                                 HistoryInfo const &second);
        static bool compareCounts(HistoryInfo const &first,
                                  HistoryInfo const &second);

};

#include "history.f"


#endif
